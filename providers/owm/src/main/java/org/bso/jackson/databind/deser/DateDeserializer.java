package org.bso.jackson.databind.deser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class DateDeserializer extends StdDeserializer<LocalDateTime> {

    public DateDeserializer() {
        super(LocalDateTime.class);
    }

    @Override
    public LocalDateTime deserialize(JsonParser p, DeserializationContext ctxt)
            throws IOException {
        long value = p.getLongValue();
        return LocalDateTime.ofEpochSecond(value, 0, ZoneOffset.UTC);
    }
}
