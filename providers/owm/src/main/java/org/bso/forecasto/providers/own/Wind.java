package org.bso.forecasto.providers.own;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Wind implements Serializable {
    private float speed;
    private float direction;

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    @JsonProperty("deg")
    public float getDirection() {
        return direction;
    }

    public void setDirection(float direction) {
        this.direction = direction;
    }
}
