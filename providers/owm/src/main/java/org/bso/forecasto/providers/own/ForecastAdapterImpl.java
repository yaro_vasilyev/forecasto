package org.bso.forecasto.providers.own;

import org.bso.forecasto.providers.ForecastAdapter;
import org.bso.forecasto.providers.ProvidedForecast;
import org.bso.forecasto.providers.TemperatureData;

import java.util.Optional;
import java.util.stream.Stream;

class ForecastAdapterImpl implements ForecastAdapter {
    private static final float KELVIN_ZERO = -273; // для ошибки

    @Override
    public Stream<TemperatureData> convert(ProvidedForecast fc) {
        OpenWeatherMapResponse forecast = (OpenWeatherMapResponse) fc;
        return forecast.getForecasts()
                .stream()
                .map(e -> new TemperatureData(
                        e.getTimeOfData(),
                        Optional.ofNullable(e.getForecastData())
                                .map(ForecastData::getTemperature).orElse(KELVIN_ZERO))
                );
    }

    @Override
    public String getLocation(ProvidedForecast fc) {
        OpenWeatherMapResponse forecast = (OpenWeatherMapResponse) fc;
        City city = forecast.getCity();
        return String.format("%s, %s (%s)", city.getName(), city.getCountry(), city.getLocation());
    }

    @Override
    public float getCurrentTemperature(ProvidedForecast fc) {
        OpenWeatherMapResponse forecast = (OpenWeatherMapResponse) fc;
        Optional<Float> temp = forecast.getForecasts()
                .stream()
                .findFirst()
                .map(Forecast::getForecastData)
                .map(ForecastData::getTemperature);
        return temp.orElse(KELVIN_ZERO);
    }
}
