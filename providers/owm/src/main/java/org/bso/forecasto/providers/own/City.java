package org.bso.forecasto.providers.own;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class City implements Serializable {
    private int id;
    private String name;
    private GeoLocation location;
    private String country;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("coord")
    public GeoLocation getLocation() {
        return location;
    }

    public void setLocation(GeoLocation coord) {
        this.location = coord;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
