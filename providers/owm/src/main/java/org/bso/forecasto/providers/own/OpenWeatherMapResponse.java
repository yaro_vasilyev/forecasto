package org.bso.forecasto.providers.own;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.bso.forecasto.providers.ProvidedForecast;

import java.io.Serializable;
import java.util.ArrayList;

public class OpenWeatherMapResponse implements ProvidedForecast, Serializable {
    private String code;
    private String message;
    private int count;
    private ArrayList<Forecast> forecasts;
    private City city;

    @JsonProperty("cod")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @JsonProperty("cnt")
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @JsonProperty("list")
    public ArrayList<Forecast> getForecasts() {
        return forecasts;
    }

    public void setForecasts(ArrayList<Forecast> forecasts) {
        this.forecasts = forecasts;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }
}
