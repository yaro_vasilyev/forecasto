package org.bso.forecasto.providers.own;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class GeoLocation implements Serializable {
    private double latitude = 0.0;
    private double longitude = 0.0;

    public GeoLocation(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public GeoLocation() {
    }

    @JsonProperty("lat")
    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    @JsonProperty("lon")
    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "lat: " + latitude +
                ", lng: " + longitude;
    }
}
