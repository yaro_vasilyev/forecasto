package org.bso.forecasto.providers.own;

import org.bso.forecasto.providers.ForecastAdapter;
import org.bso.forecasto.providers.ForecastProvider;
import org.bso.forecasto.providers.ForecastProviderException;
import org.bso.forecasto.providers.ProvidedForecast;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

public class OpenWeatherMapForecastProvider implements ForecastProvider {
    private static final String GET_FORECAST_URL = "http://api.openweathermap.org/data/2.5/forecast?q={q}&appid={appid}&units=metric";
    private static final Logger LOG = LoggerFactory.getLogger(OpenWeatherMapForecastProvider.class);
    private static final ForecastAdapterImpl ADAPTER = new ForecastAdapterImpl();

    public static final String CACHE_NAME = "owmForecastProvider.getForecast";
    public static final String PROVIDER_NAME = "OpenWeatherMap";

    @Value("${openWeatherMap.appid}")
    protected String ownKey;

    @Cacheable(
            cacheNames = CACHE_NAME,
            key = "{T(org.bso.forecasto.providers.own.OpenWeatherMapForecastProvider).PROVIDER_NAME, #countryCode, #city}"
    )
    @Override
    public ProvidedForecast getForecast(String countryCode, String city) throws ForecastProviderException {
        try {
            return new RestTemplate().getForObject(
                    GET_FORECAST_URL,
                    OpenWeatherMapResponse.class,
                    new HashMap<String, Object>() {{
                        put("q", String.format("%s,%s", city, countryCode));
                        put("appid", ownKey);
                    }}
            );
        } catch (Exception ex) {
            LOG.error("getForecast: {}", ex.getMessage());
            throw new ForecastProviderException(ex.getMessage(), ex);
        }
    }

    @Override
    public ForecastAdapter getAdapter() {
        return ADAPTER;
    }

    @Override
    public String getType() {
        return PROVIDER_NAME;
    }
}
