package org.bso.forecasto.providers.own;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ForecastData implements Serializable{
    private float temperature;
    private float minTemperature;
    private float maxTemperature;
    private float pressure;
    private float seaLevelPressure;
    private float groundLevelPressure;
    private int humidity;

    @JsonProperty("temp")
    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    @JsonProperty("temp_min")
    public float getMinTemperature() {
        return minTemperature;
    }

    public void setMinTemperature(float minTemperature) {
        this.minTemperature = minTemperature;
    }

    @JsonProperty("temp_max")
    public float getMaxTemperature() {
        return maxTemperature;
    }

    public void setMaxTemperature(float maxTemperature) {
        this.maxTemperature = maxTemperature;
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }

    @JsonProperty("sea_level")
    public float getSeaLevelPressure() {
        return seaLevelPressure;
    }

    public void setSeaLevelPressure(float seaLevelPressure) {
        this.seaLevelPressure = seaLevelPressure;
    }

    @JsonProperty("grnd_level")
    public float getGroundLevelPressure() {
        return groundLevelPressure;
    }

    public void setGroundLevelPressure(float groundLevelPressure) {
        this.groundLevelPressure = groundLevelPressure;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }
}
