package org.bso.forecasto.providers.own;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.bso.jackson.databind.deser.DateDeserializer;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class Forecast implements Serializable {
    private LocalDateTime timeOfData;
    private ForecastData forecastData;
    private ArrayList<Weather> weather;
    private Clouds clouds;
    private Wind wind;

    @JsonProperty("dt")
    @JsonDeserialize(using = DateDeserializer.class)
    public LocalDateTime getTimeOfData() {
        return timeOfData;
    }

    public void setTimeOfData(LocalDateTime date) {
        this.timeOfData = date;
    }

    @JsonProperty("main")
    public ForecastData getForecastData() {
        return forecastData;
    }

    public void setForecastData(ForecastData forecastData) {
        this.forecastData = forecastData;
    }

    public ArrayList<Weather> getWeather() {
        return weather;
    }

    public void setWeather(ArrayList<Weather> weather) {
        this.weather = weather;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }
}
