package org.bso.forecasto.providers.own;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Clouds implements Serializable {
    private int percent;

    @JsonProperty("all")
    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }
}
