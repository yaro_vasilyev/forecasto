package org.bso.forecasto.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class ForecastController {
    @GetMapping("/test")
    String test(Model model) {
        return "test";
    }
}
