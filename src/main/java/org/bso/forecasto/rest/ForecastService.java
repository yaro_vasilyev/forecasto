package org.bso.forecasto.rest;

import org.bso.forecasto.providers.*;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.util.Assert;
import org.springframework.util.MimeTypeUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Locale;
import java.util.Optional;

import static java.util.Comparator.comparing;

@Validated
@RestController()
@RequestMapping(value = "/api/forecast", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
public class ForecastService implements MessageSourceAware {

    private static final String SERVICE_UNAVAILABLE_KEY = "forecastService.unavailable";
    private static final Logger LOG = LoggerFactory.getLogger(ForecastService.class);
    public static final String CACHE_NAME = "forecastService.get3DayWeather";

    private final ForecastProvider forecastProvider;
    private final ForecastAdapter forecastAdapter;

    private MessageSource messageSource;

    @Autowired
    public ForecastService(ForecastProvider forecastProvider) {
        Assert.notNull(forecastProvider, "forecastProvider should not be null.");
        this.forecastProvider = forecastProvider;
        this.forecastAdapter = forecastProvider.getAdapter();
        Assert.notNull(this.forecastAdapter, "forecastProvider bean returns null forecastAdapter!");
    }

    @Cacheable(
            cacheNames = CACHE_NAME,
            key = "{#countryCode, #city}"
    )
    @GetMapping("/3day")
    public ForecastResponse get3DayWeather(
            @RequestParam("country") @Size(min = 2, max = 2) String countryCode,
            @RequestParam("city") @NotEmpty String city) {

        LOG.info("Incoming request for {}, {}", city, countryCode);

        // часовые пояса не считаем
        LocalDateTime futureMoment = LocalDateTime.now().plusDays(3);
        TemperatureData fakeForecast = new TemperatureData(futureMoment, 0);
        try {
            ProvidedForecast providedForecast = forecastProvider.getForecast(countryCode, city);

            Optional<TemperatureData> forecastWithMinimumTemp = forecastAdapter.convert(providedForecast)
                    .filter(tf -> comparing(TemperatureData::getDateTime).compare(tf, fakeForecast) <= 0)
                    .min(comparing(TemperatureData::getTemperature));

            TemperatureData forecast = forecastWithMinimumTemp.orElseThrow(ForecastProviderException::new);
            return ForecastResponse.create()
                    .minTemperature(forecast.getTemperature())
                    .currentTemperature(forecastAdapter.getCurrentTemperature(providedForecast))
                    .when(forecast.getDateTime())
                    .where(forecastAdapter.getLocation(providedForecast))
                    .build();
        } catch (ForecastProviderException e) {
            return ForecastResponse.create()
                    .error(messageSource.getMessage(SERVICE_UNAVAILABLE_KEY, null, "Service unavailable", Locale.getDefault()))
                    .build();
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }
}
