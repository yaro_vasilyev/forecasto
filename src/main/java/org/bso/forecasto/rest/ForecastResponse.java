package org.bso.forecasto.rest;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.time.LocalDateTime;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ForecastResponse implements Serializable {
    private static final long serialVersionUID = -8090782524125552357L;

    private boolean success = true;
    private Payload payload;
    private String error;

    public static class Payload implements Serializable {
        private static final long serialVersionUID = -8190546377991825029L;

        private float minTemperature;
        private float currentTemperature;
        private LocalDateTime dateTime;
        private String location;

        @JsonProperty("min")
        public float getMinTemperature() {
            return minTemperature;
        }

        @JsonProperty("current")
        public float getCurrentTemperature() {
            return currentTemperature;
        }

        public void setMinTemperature(float minTemperature) {
            this.minTemperature = minTemperature;
        }

        public void setCurrentTemperature(float currentTemperature) {
            this.currentTemperature = currentTemperature;
        }

        @JsonProperty("when")
        public LocalDateTime getDateTime() {
            return this.dateTime;
        }

        public void setDateTime(LocalDateTime when) {
            this.dateTime = when;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        @JsonProperty("where")
        public String getLocation() {
            return location;
        }
    }

    @JsonProperty("success")
    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    @JsonProperty("payload")
    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    @JsonProperty("error")
    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public static ForecastResponse success(float minTemp) {
        ForecastResponse response = new ForecastResponse();
        response.setSuccess(true);
        Payload payload = new Payload();
        payload.setMinTemperature(minTemp);
        response.setPayload(payload);
        return response;
    }

    public interface ForecastResponseBuilder {
        ForecastResponseBuilder minTemperature(float temperature);

        ForecastResponseBuilder currentTemperature(float temperature);

        ForecastResponseBuilder error(String error);

        ForecastResponse build();

        ForecastResponseBuilder when(LocalDateTime dateTime);

        ForecastResponseBuilder where(String location);
    }

    public static ForecastResponseBuilder create() {
        return new ForecastResponseBuilderImpl();
    }

    static class ForecastResponseBuilderImpl implements ForecastResponseBuilder {
        private Float minTemp;
        private Float currentTemp;
        private boolean success;
        private String error;
        private LocalDateTime when;
        private String where;

        @Override
        public ForecastResponseBuilder minTemperature(float temp) {
            this.minTemp = temp;
            this.success = true;
            this.error = null;
            return this;
        }

        @Override
        public ForecastResponseBuilder currentTemperature(float temperature) {
            this.currentTemp = temperature;
            this.success = true;
            this.error = null;
            return this;
        }

        @Override
        public ForecastResponseBuilder error(String error) {
            this.minTemp = null;
            this.currentTemp = null;
            this.success = false;
            this.error = error;
            return this;
        }

        @Override
        public ForecastResponse build() {
            ForecastResponse response = new ForecastResponse();
            response.setSuccess(this.success);
            if (this.success) {
                Payload payload = new Payload();
                payload.setMinTemperature(this.minTemp);
                payload.setCurrentTemperature(this.currentTemp);
                payload.setDateTime(this.when);
                payload.setLocation(this.where);
                response.setPayload(payload);
            } else {
                response.setError(this.error);
            }
            return response;
        }

        @Override
        public ForecastResponseBuilder when(LocalDateTime dateTime) {
            this.when = dateTime;
            return this;
        }

        @Override
        public ForecastResponseBuilder where(String location) {
            this.where = location;
            return this;
        }
    }
}
