package org.bso.forecasto.providers;

public abstract class KnownProviders {
    public static final String OPEN_WEATHER_MAP = "OpenWeatherMap";

    private KnownProviders() {
    }
}
