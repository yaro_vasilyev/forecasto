package org.bso.forecasto;

import org.bso.forecasto.providers.ForecastProvider;
import org.bso.forecasto.providers.own.OpenWeatherMapForecastProvider;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.ResourceBundleMessageSource;

@EnableCaching
@SpringBootApplication
public class ForecastoApplication {

    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasenames("messages");
        return messageSource;
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
    ForecastProvider forecastProvider() {
        return new OpenWeatherMapForecastProvider();
    }

    public static void main(String[] args) {
        SpringApplication.run(ForecastoApplication.class, args);
    }
}
