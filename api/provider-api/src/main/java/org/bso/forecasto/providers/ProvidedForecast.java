package org.bso.forecasto.providers;

/**
 * ProvidedForecast marker interface for data returned by forecast provider
 */
public interface ProvidedForecast {
}
