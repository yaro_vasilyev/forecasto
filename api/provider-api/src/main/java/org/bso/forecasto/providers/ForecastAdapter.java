package org.bso.forecasto.providers;

import java.util.stream.Stream;

public interface ForecastAdapter {
    Stream<TemperatureData> convert(ProvidedForecast providedForecast);
    String getLocation(ProvidedForecast providedForecast);
    float getCurrentTemperature(ProvidedForecast providedForecast);
}
