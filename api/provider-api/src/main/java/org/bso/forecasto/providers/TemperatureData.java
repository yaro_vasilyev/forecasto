package org.bso.forecasto.providers;

import java.time.LocalDateTime;

public class TemperatureData {
    private final LocalDateTime dateTime;
    private final float temperature;

    public TemperatureData(LocalDateTime dateTime, float temperature) {
        if(dateTime == null)
            throw new IllegalArgumentException("dateTime can't be null");

        this.dateTime = dateTime;
        this.temperature = temperature;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public float getTemperature() {
        return temperature;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TemperatureData that = (TemperatureData) o;

        if (Float.compare(that.temperature, temperature) != 0) return false;
        return dateTime.equals(that.dateTime);
    }

    @Override
    public int hashCode() {
        int result = dateTime.hashCode();
        result = 31 * result + (temperature != +0.0f ? Float.floatToIntBits(temperature) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TemperatureData{" +
                "dateTime=" + dateTime +
                ", temperature=" + temperature +
                '}';
    }
}
