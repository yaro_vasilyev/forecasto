package org.bso.forecasto.providers;

public class ForecastProviderException extends Exception {
    public ForecastProviderException() {
        super();
    }

    public ForecastProviderException(String message) {
        super(message);
    }

    public ForecastProviderException(String message, Throwable cause) {
        super(message, cause);
    }

    public ForecastProviderException(Throwable cause) {
        super(cause);
    }
}
