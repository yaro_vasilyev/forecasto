package org.bso.forecasto.providers;

public interface ForecastProvider {
    ProvidedForecast getForecast(String countryCode, String city)
            throws ForecastProviderException;

    ForecastAdapter getAdapter();

    String getType();
}
