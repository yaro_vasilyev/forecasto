import {WEATHER_SERVICE_URL} from '../Const';
import {Weather, WeatherService} from '../Models';
import {GET} from '../utils/FetchUtils';

export class WeatherServiceImpl implements WeatherService {
    get3day(country: string, city: string): Promise<Weather | Error> {
        return GET<Weather>(WEATHER_SERVICE_URL, {country, city});
    }
}
