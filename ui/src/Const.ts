export const ACTION_GET_WEATHER = 'GET_WEATHER';
export const ACTION_CLEAR = 'CLEAR';
export const ACTION_SELECT_HISTORY_ITEM = 'SELECT_HISTORY_ITEM';
export const ACTION_SET_LOCATION = 'SET_LOCATION';

export const WEATHER_SERVICE_ADDR = '/api/forecast/3day';
export const WEATHER_SERVICE_URL = window.location.origin + WEATHER_SERVICE_ADDR;
