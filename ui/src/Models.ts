export interface ApplicationState {
    country: string;
    city: string;
    selectedForecast: WeatherHistoryItem | null;
    history: WeatherHistoryItem[];
    isFetching: false;
    error: string | null;
}

export interface Actions {
    getWeather: (country: string, city: string) => void;
    clear: () => void;
    selectHistoryForecast: (index: number) => void;
    setLocation: (country: string, city: string) => void;
}

export interface Weather {
    current: number;
    min: number;
    when: string;
    where: string;
}

export interface WeatherHistoryItem extends Weather {
    fetchMoment: number;
}

export interface WeatherService {
    get3day(country: string, city: string): Promise<Weather | Error>;
}
