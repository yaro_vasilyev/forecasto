import * as React from 'react';
import * as moment from 'moment';
import * as classNames from 'classnames';
import 'moment/locale/ru';
import {isFunction, isUndefined} from 'lodash';
import {Temperature} from './Temperature';

interface Props {
    datetime: string;
    location: string;
    currentTemp: string;
    minTemp: string;
    historyIndex: number;
    selected: boolean;
    onClick: (ix: number) => void;
}

export class HistoryItem extends React.Component<Props, {}> {
    handleClick = (): void => {
        const {onClick, historyIndex} = this.props;
        !isUndefined(onClick) && isFunction(onClick) && onClick(historyIndex);
    };

    render() {
        const {datetime, location, minTemp, currentTemp, selected} = this.props;
        return (
            <tr onClick={this.handleClick} className={classNames({selected})}>
                <td>{moment(datetime).format('LLLL')}</td>
                <td>{location}</td>
                <td><Temperature temp={currentTemp}/></td>
                <td><Temperature temp={minTemp}/></td>
            </tr>
        );
    }
}