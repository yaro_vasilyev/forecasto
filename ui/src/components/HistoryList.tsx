import '../style/HistoryList.css';
import * as React from 'react';
import {Button, ButtonGroup, ButtonToolbar, Clearfix, Table} from 'react-bootstrap';
import {HistoryItem} from './HistoryItem';
import {WeatherHistoryItem} from '../Models';
import {isFunction, isUndefined} from 'lodash';

interface Props {
    history: WeatherHistoryItem[];
    onClear: () => void;
    onSelectHistoryItem: (itemIndex: number) => void;
}

interface State {
    selectedItemIndex?: number | null;
}

export class HistoryList extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {};
    }

    componentWillReceiveProps(nextProps: Props) {
        if (nextProps.history.length !== this.props.history.length) {
            this.setState({
                selectedItemIndex: 0
            });
        }
    }

    handleSelectItem = (historyIndex: number): void => {
        const {onSelectHistoryItem} = this.props;
        if (!isUndefined(onSelectHistoryItem) && isFunction(onSelectHistoryItem)) {
            this.setState({
                ...this.state,
                selectedItemIndex: historyIndex
            }, () => onSelectHistoryItem(historyIndex));
        }
    };

    render() {
        const {history, onClear} = this.props;
        const {selectedItemIndex} = this.state;

        return (
            <div>
                <ButtonToolbar>
                    <ButtonGroup className="pull-right">
                        {
                            history.length > 0 && <Button onClick={onClear} bsStyle="danger">Clear</Button>
                        }
                    </ButtonGroup>
                </ButtonToolbar>
                <Clearfix/>
                <Table className="history-list">
                    <thead>
                    <tr>
                        <th>When</th>
                        <th>Where</th>
                        <th>Current T⁰</th>
                        <th>Minimal T⁰</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        history.map((whi: WeatherHistoryItem, ix: number) =>
                            <HistoryItem
                                key={whi.fetchMoment}
                                datetime={whi.when}
                                location={whi.where}
                                currentTemp={whi.current.toString()}
                                minTemp={whi.min.toString()}
                                selected={selectedItemIndex === ix}
                                historyIndex={ix}
                                onClick={this.handleSelectItem}
                            />
                        )
                    }
                    </tbody>
                </Table>
            </div>
        );
    }
}