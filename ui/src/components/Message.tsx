
import * as React from 'react';
import {Alert} from 'react-bootstrap';

interface Props {
    message: string | null | undefined;
}

export function Message(props: Props) {
    const {message} = props;

    if (message) {
        return <Alert bsStyle="danger">{message}</Alert>
    }

    return null;
}