
import * as React from 'react';

export function Temperature ({temp, measure = 'C'}: {temp: number | string, measure?: string}): JSX.Element | null {
    return <span>{temp} ⁰{measure}</span>
}