import '../style/AppHeader.css';
import * as React from 'react';
import * as classNames from 'classnames';

const logo = require('../sun.svg');

export function AppHeader({isLoading = false}: { isLoading?: boolean }): JSX.Element {
    return (
        <div className="app-header app-section">
            <div>
                <img
                    src={logo}
                    className={classNames({
                        [`app-logo`]: true,
                        [`app-logo-animate`]: isLoading
                    })}
                    alt="logo"
                />
                <h2>Погода</h2>
            </div>
        </div>
    );
}