import * as React from 'react';
import {
    Panel,
    ListGroup,
    ListGroupItem
} from 'react-bootstrap';
import {WeatherHistoryItem} from '../Models';
import * as moment from 'moment';
import 'moment/locale/ru';
import {Temperature} from './Temperature';

interface Props {
    forecast: WeatherHistoryItem | null;
}

export function ForecastDisplay(props: Props) {

    if (!props.forecast) {
        return null;
    }

    const {
        fetchMoment,
        when,
        where,
        min,
        current
    } = props.forecast;

    // const format = (arg: string | number): string => moment(arg).format('LLLL');

    const Header = <h3>Forecast {moment(fetchMoment).format('LL LTS')}</h3>;

    return props.forecast && (
        <Panel header={Header} bsStyle="success" className="app-section">
            <ListGroup fill>
                <ListGroupItem>Current: <Temperature temp={current}/></ListGroupItem>
                <ListGroupItem>Location: {where}</ListGroupItem>
                <ListGroupItem>When (min temp): {moment(when).format('LLLL')}</ListGroupItem>
                <ListGroupItem>Min temperature: <Temperature temp={min}/></ListGroupItem>
            </ListGroup>
        </Panel>
    );
}