import * as React from 'react';
import {Button, ControlLabel, Form, FormControl, FormGroup} from 'react-bootstrap';
import {FormEvent} from 'react';
import {isFunction, isUndefined} from 'lodash';

interface Props {
    country: string;
    city: string;
    onGetWeather?: () => void;
    onLocationChange?: (country: string, city: string) => void;
    enabled: boolean;
}

export class ForecastForm extends React.Component<Props, {}> {

    countryControl: HTMLInputElement;
    cityControl: HTMLInputElement;

    handleLocationChange = (e: FormEvent<FormControl>): void => {
        const {onLocationChange} = this.props;
        !isUndefined(onLocationChange) &&
        isFunction(onLocationChange) &&
        onLocationChange(this.countryControl.value, this.cityControl.value);
    };

    handleSubmit = (e: FormEvent<FormControl>): void => {
        const {onGetWeather} = this.props;
        e.preventDefault();
        !isUndefined(onGetWeather) && isFunction(onGetWeather) && onGetWeather();
    };

    render() {
        const {country, city, enabled} = this.props;

        return (
            <Form inline className="app-section" onSubmit={this.handleSubmit}>
                <FormGroup controlId="city">
                    <ControlLabel>City</ControlLabel>
                    {' '}
                    <FormControl
                        type="text"
                        value={city}
                        onChange={this.handleLocationChange}
                        inputRef={e => { this.cityControl = e; }}
                        disabled={!enabled}
                    />
                </FormGroup>
                {' '}
                <FormGroup controlId="country" disabled={!enabled}>
                    <ControlLabel>Country</ControlLabel>
                    {' '}
                    <FormControl
                        type="text"
                        placeholder="Country code (eg RU, EN, US)"
                        value={country}
                        onChange={this.handleLocationChange}
                        inputRef={e => { this.countryControl = e; }}
                        disabled={!enabled}
                    />
                </FormGroup>
                {' '}
                <FormGroup disabled={!enabled}>
                    <Button
                        type="submit"
                        bsStyle="success"
                        disabled={!enabled}
                    >
                        Get Weather!
                    </Button>
                </FormGroup>
            </Form>
        );
    }
}