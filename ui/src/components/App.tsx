import '../style/App.css';

import * as React from 'react';
import {
    isUndefined,
    isFunction
} from 'lodash';
import {
    connect,
    Dispatch
} from 'react-redux';

import {ApplicationState, WeatherHistoryItem} from '../Models';
import {ActionsImpl} from '../reducers/Actions';
import {HistoryList} from './HistoryList';
import {ForecastDisplay} from './ForecastDisplay';
import {ForecastForm} from './ForecastForm';
import {Message} from './Message';
import {AppHeader} from './AppHeader';

interface StateProps {
    history: WeatherHistoryItem[];
    forecastToDisplay: WeatherHistoryItem | null;
    country: string;
    city: string;
    isLoading: boolean;
    error?: string | null;
}

interface DispatchProps {
    onWeatherGet: (country: string, city: string) => void;
    onClear: () => void;
    onSelectHistoryItem: (itemIndex: number) => void;
    onLocationChange: (country: string, city: string) => void;
}

type TProps = StateProps & DispatchProps;

class App extends React.Component<TProps> {

    handleSelectHistoryItem = (itemIndex: number): void => {
        const {onSelectHistoryItem} = this.props;
        !isUndefined(onSelectHistoryItem) && isFunction(onSelectHistoryItem) && onSelectHistoryItem(itemIndex);
    };

    handleGetWeather = (): void => {
        const {onWeatherGet, country, city} = this.props;
        !isUndefined(onWeatherGet) &&
        isFunction(onWeatherGet) &&
        onWeatherGet(country ? country : '', city ? city : '');
    };

    render() {
        const {
            history,
            forecastToDisplay,
            country,
            city,
            error,
            isLoading,
            onLocationChange,
            onClear
        } = this.props;

        return (
            <div className="app">
                <AppHeader isLoading={isLoading}/>
                <ForecastForm
                    country={country}
                    city={city}
                    onLocationChange={onLocationChange}
                    onGetWeather={this.handleGetWeather}
                    enabled={!isLoading}
                />
                <Message message={error}/>
                <ForecastDisplay forecast={forecastToDisplay}/>
                <HistoryList history={history} onClear={onClear} onSelectHistoryItem={this.handleSelectHistoryItem}/>
                <footer>
                    <a href="http://www.onlinewebfonts.com">oNline Web Fonts</a>
                </footer>
            </div>
        );

    }
}

function mapStateToProps(state: ApplicationState): StateProps {
    return {
        history: state.history,
        forecastToDisplay: state.selectedForecast,
        country: state.country,
        city: state.city,
        isLoading: state.isFetching ? state.isFetching : false,
        error: state.error
    };
}

function mapDispatchToProps(dispatch: Dispatch<any>): DispatchProps {
    const actions = new ActionsImpl(dispatch);

    return {
        onWeatherGet: actions.getWeather,
        onClear: actions.clear,
        onSelectHistoryItem: actions.selectHistoryForecast,
        onLocationChange: actions.setLocation
    } as DispatchProps;
}

const connectedComponent = connect(
    mapStateToProps,
    mapDispatchToProps)(App);

export {connectedComponent as App};
