import 'bootstrap/dist/css/bootstrap.css'
import './style/index.css';

import * as React from 'react';
import * as ReactDOM from 'react-dom';

import {App} from './components/App';

import {
    applyMiddleware,
    // compose,
    createStore
} from 'redux';
import ReduxThunk from 'redux-thunk';
import {
    Provider,
    Store
} from 'react-redux';

import {ApplicationState} from './Models';
import {reducer} from './reducers/Reducer';

const store: Store<ApplicationState> = createStore(
    reducer,
    applyMiddleware(ReduxThunk)
);
//
// const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
// const store = composeEnhancers(
//     applyMiddleware(ReduxThunk),
//     applyMiddleware(promiseMiddleware({promiseTypeSuffixes: ['BEGIN', 'SUCCESS', 'FAILURE']}))
// )(createStore)(reducer);

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root') as HTMLElement
);
