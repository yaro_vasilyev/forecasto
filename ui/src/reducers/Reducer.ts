import {Action, handleActions} from 'redux-actions';
import {ACTION_CLEAR, ACTION_GET_WEATHER, ACTION_SELECT_HISTORY_ITEM, ACTION_SET_LOCATION} from '../Const';
import {ApplicationState, Weather, WeatherHistoryItem} from '../Models';

export interface SelectHistoryItemPayload {
    index: number;
}

export interface SetLocationPayload {
    country: string;
    city: string;
}

function getInitialState(): ApplicationState {
    return {
        country: 'RU',
        city: '',
        history: <WeatherHistoryItem[]> [],
        selectedForecast: null,
        isFetching: false,
        error: null
    };
}

export const reducer = handleActions<ApplicationState>({
    [ACTION_CLEAR]: state => ({
        ...getInitialState(),
        city: state.city,
        country: state.country
    }),
    [`${ACTION_GET_WEATHER}_BEGIN`]: state => ({
        ...state,
        isFetching: true,
        error: null
    }),
    [`${ACTION_GET_WEATHER}_SUCCESS`]: (state, action: Action<Weather>) => ({
        ...state,
        isFetching: false,
        history: [
            {
                ...action.payload,
                fetchMoment: new Date().getTime()
            },
            ...state.history
        ],
        selectedForecast: action.payload
    }),
    [`${ACTION_GET_WEATHER}_FAILURE`]: (state, action: Action<Error>) => ({
        ...state,
        selectedForecast: null,
        error: action.payload && action.payload.message,
        isFetching: false
    }),
    [`${ACTION_SELECT_HISTORY_ITEM}`]: (state, action: Action<SelectHistoryItemPayload>) => ({
        ...state,
        selectedForecast: action.payload ? state.history[action.payload.index] : null
    }),
    [ACTION_SET_LOCATION]: (state, action: Action<SetLocationPayload>) => ({
        ...state,
        country: action.payload ? action.payload.country : '',
        city: action.payload ? action.payload.city : ''
    })
}, getInitialState());
