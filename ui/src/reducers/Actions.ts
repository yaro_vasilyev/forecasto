import {Dispatch} from 'react-redux';
import {
    ACTION_CLEAR,
    ACTION_GET_WEATHER,
    ACTION_SELECT_HISTORY_ITEM, ACTION_SET_LOCATION
} from '../Const';
import {
    Actions,
    Weather,
    WeatherService
} from '../Models';
import {WeatherServiceImpl} from '../services/WeatherServiceImpl';
import {dispatchAction, dispatchAsync} from '../utils/DispatchUtils';
import {SelectHistoryItemPayload, SetLocationPayload} from './Reducer';

export class ActionsImpl implements Actions {
    readonly weatherService: WeatherService;

    constructor(private readonly dispatch: Dispatch<{}>) {
        this.weatherService = new WeatherServiceImpl();
    }

    getWeather = (country: string, city: string): void => {
        dispatchAsync<Weather, Error>(this.dispatch,
            ACTION_GET_WEATHER,
            () => {
                return this.weatherService.get3day(country, city)
                    .then((weather: Weather) => {
                            console.log('Weather (fulfill):', weather);
                            return weather;
                        },
                        (reason: Error) => {
                            console.log('Weather (reject):', reason);
                            return Promise.reject(reason);
                        });
            });
    };

    clear = (): void => {
        dispatchAction(this.dispatch, ACTION_CLEAR);
    };

    selectHistoryForecast = (index: number): void => {
        dispatchAction(this.dispatch, ACTION_SELECT_HISTORY_ITEM,
            () => <SelectHistoryItemPayload> {index});
    };

    setLocation = (country: string, city: string): void => {
        dispatchAction(this.dispatch, ACTION_SET_LOCATION, {
            country,
            city
        } as SetLocationPayload);
    };
}
