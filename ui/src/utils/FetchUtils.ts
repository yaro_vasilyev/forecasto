export interface QueryParams {
    [name: string]: string;
}

export interface ServiceResponse<TPayload> extends Response {
    success: boolean;
    error?: string;
    payload: TPayload;
}

export function GET<TPayload>(urlStr: string, queryParams?: QueryParams): Promise<TPayload | Error> {
    const url = new URL(urlStr);

    if (queryParams) {
        Object.keys(queryParams).forEach(key =>
            url.searchParams.append(key, queryParams[key]));
    }

    return fetch(url.toString()).then((response: Response) => {
        if (response.status >= 200 && response.status < 300) {
            return response.json();
        }
        return Promise.reject(new Error(response.status.toString()));
    }).then(
        (json: ServiceResponse<TPayload>) => {
            if (json.success) {
                return Promise.resolve(json.payload);
            } else {
                return Promise.reject(new Error(json.error));
            }
        });
}
