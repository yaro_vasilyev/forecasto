import {isFunction} from 'lodash';
import {Dispatch} from 'redux';
import {createAction} from 'redux-actions';

export function dispatchBegin(dispatch: Dispatch<{}>, actionType: string) {
    dispatch(createAction(`${actionType}_BEGIN`)());
}

export function dispatchSuccess<TPayload>(dispatch: Dispatch<{}>,
                                          actionType: string, payloadOrCreator: (() => TPayload) | TPayload): void {
    dispatch(createAction(`${actionType}_SUCCESS`,
        isFunction(payloadOrCreator) ?
            payloadOrCreator :
            () => payloadOrCreator)());
}

export function dispatchFailure<TReason>(dispatch: Dispatch<{}>,
                                         actionType: string, reasonOrCreator: (() => TReason) | TReason) {
    dispatch(createAction(`${actionType}_FAILURE`,
        isFunction(reasonOrCreator) ?
            reasonOrCreator :
            () => reasonOrCreator)()
    );
}

export function dispatchAsync<TPayload, TReason>(dispatch: Dispatch<{}>,
                                                 actionType: string, asyncCall: () => Promise<TPayload | TReason>) {
    dispatchBegin(dispatch, actionType);

    const p = asyncCall();
    if (p) {
        return p.then((resp: TPayload) => {
                dispatchSuccess(dispatch, actionType, resp);
                return resp;
            },
            reason => {
                dispatchFailure(dispatch, actionType, reason);
                return reason;
            });
    } else {
        return Promise.reject(new Error('Strange!'));
    }
}

export function dispatchAction<TPayload>(dispatch: Dispatch<{}>, actionType: string,
                                         payloadOrCreator?: (() => TPayload) | TPayload) {
    dispatch(createAction(actionType,
        payloadOrCreator && isFunction(payloadOrCreator) ?
            payloadOrCreator :
            () => payloadOrCreator)()
    );
}
